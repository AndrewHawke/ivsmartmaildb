/****** Object:  Table [dbo].[IVProductImage]    Script Date: 3/4/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVProductImage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IVProductId] [int] NOT NULL,
	[Position] [int] NULL,
	[IVSrc] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NULL,
	[UpdatedAt] [datetime] NULL,
	[PimImageId] [int] NULL,
 CONSTRAINT [PK_IVProductImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[IVProductImage]  WITH CHECK ADD  CONSTRAINT [FK_IVProductImage_IVProduct] FOREIGN KEY([IVProductId])
REFERENCES [dbo].[IVProduct] ([Id])
GO

ALTER TABLE [dbo].[IVProductImage] CHECK CONSTRAINT [FK_IVProductImage_IVProduct]
GO

