/****** Object:  Table [dbo].[IVCustProdTypeFreq]    Script Date: 3/4/2019 6:29:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVCustProdTypeFreq](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ProductTypeId] [int] NOT NULL,
	[LastPurchaseDate] [date] NOT NULL,
 CONSTRAINT [PK_IVCustProdTypeFreq] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

