/****** Object:  Table [dbo].[IVMktMail]    Script Date: 3/4/2019 6:58:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVMktMail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[GridPattern] [nvarchar](50) NULL,
	[BckImage1] [nvarchar](200) NULL,
	[BckImage2] [nvarchar](200) NULL,
	[BckImage3] [nvarchar](100) NULL,
	[MailText] [nvarchar](max) NULL,
	[MailDistListId] [int] NULL,
	[CustomerGroup] [nvarchar](500) NULL,
	[Preview] [nvarchar](200) NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[CreatedAt] [date] NULL,
	[MailBckId] [int] NULL,
	[StartingAfterEmail] [nvarchar](100) NULL,
 CONSTRAINT [PK_IVMktMail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[IVMktMail]  WITH CHECK ADD  CONSTRAINT [FK_IVMktMail_IVMailDistList] FOREIGN KEY([MailDistListId])
REFERENCES [dbo].[IVMailDistList] ([Id])
GO

ALTER TABLE [dbo].[IVMktMail] CHECK CONSTRAINT [FK_IVMktMail_IVMailDistList]
GO

ALTER TABLE [dbo].[IVMktMail]  WITH CHECK ADD  CONSTRAINT [FK_IVMktMail_MailBckId] FOREIGN KEY([MailBckId])
REFERENCES [dbo].[IVMailBckImage] ([Id])
GO

ALTER TABLE [dbo].[IVMktMail] CHECK CONSTRAINT [FK_IVMktMail_MailBckId]
GO

