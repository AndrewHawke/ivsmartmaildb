/****** Object:  Table [dbo].[IVCustFavBrand]    Script Date: 3/4/2019 6:24:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVCustFavBrand](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BrandId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CategoryId] [int] NULL,
	[ValuePrcnt] [int] NOT NULL,
	[Instore] [bit] NOT NULL,
	[Value] [int] NOT NULL
) ON [PRIMARY]
GO

