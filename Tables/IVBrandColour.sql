﻿CREATE TABLE [dbo].[IVBrandColour]
(
	[Id] INT NOT NULL IDENTITY, 
    [IVBrandId] INT NOT NULL, 
    [BrandColour] NVARCHAR(MAX) NOT NULL, 
    [SystemColoursCsv] NVARCHAR(MAX) NOT NULL,
	CONSTRAINT [PK_IVBrandColours] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
    CONSTRAINT [FK_IVBrandColour_IVBrand] FOREIGN KEY ([IVBrandId]) REFERENCES [IVBrand]([Id])
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
