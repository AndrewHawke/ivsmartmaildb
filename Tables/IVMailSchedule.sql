/****** Object:  Table [dbo].[IVMailSchedule]    Script Date: 3/4/2019 6:58:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVMailSchedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IVRuleSetId] [int] NULL,
	[Schedule] [nvarchar](20) NULL
) ON [PRIMARY]
GO

