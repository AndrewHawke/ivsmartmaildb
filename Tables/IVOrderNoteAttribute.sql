/****** Object:  Table [dbo].[IVOrderNoteAttribute]    Script Date: 3/4/2019 7:01:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVOrderNoteAttribute](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IVOrderId] [int] NULL,
	[Name] [nvarchar](100) NULL,
	[Value] [nvarchar](200) NULL,
 CONSTRAINT [PK_IVOrderNoteAttribute] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[IVOrderNoteAttribute]  WITH CHECK ADD  CONSTRAINT [FK_IVOrderNoteAttribute_IVOrder] FOREIGN KEY([IVOrderId])
REFERENCES [dbo].[IVOrder] ([Id])
GO

ALTER TABLE [dbo].[IVOrderNoteAttribute] CHECK CONSTRAINT [FK_IVOrderNoteAttribute_IVOrder]
GO

