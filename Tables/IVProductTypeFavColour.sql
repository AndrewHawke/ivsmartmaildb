/****** Object:  Table [dbo].[IVProductTypeFavColour]    Script Date: 3/4/2019 7:03:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVProductTypeFavColour](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductTypeId] [int] NOT NULL,
	[ColourId] [int] NOT NULL,
	[ValuePrcnt] [int] NOT NULL,
	[ValueType] [char](1) NOT NULL,
	[DisciplineId] [int] NULL,
	[Value] [int] NOT NULL
) ON [PRIMARY]
GO

