/****** Object:  Table [dbo].[IVTaxTypes]    Script Date: 3/4/2019 7:04:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVTaxTypes](
	[Id] [int] NOT NULL,
	[Title] [nvarchar](200) NULL,
	[Rate] [numeric](14, 4) NULL,
	[Type] [nvarchar](50) NULL,
	[OrderId] [int] NULL,
	[LineItemId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

