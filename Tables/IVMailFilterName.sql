/****** Object:  Table [dbo].[IVMailFilterName]    Script Date: 3/4/2019 6:58:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVMailFilterName](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsPrimaryFilter] [bit] NULL,
	[IsSecondFilter] [bit] NULL,
 CONSTRAINT [PK_IVMailFilterName] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

