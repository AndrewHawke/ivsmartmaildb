/****** Object:  Table [dbo].[IVJobLog]    Script Date: 3/4/2019 6:57:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVJobLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Task] [nvarchar](200) NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[MessageType] [char](1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

