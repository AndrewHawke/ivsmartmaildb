/****** Object:  Table [dbo].[IVMailRule]    Script Date: 3/4/2019 6:58:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVMailRule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[IVMktMailId] [int] NOT NULL,
	[PrimeFilterId] [int] NULL,
	[Location] [int] NULL,
	[PFValue] [nvarchar](500) NULL
) ON [PRIMARY]
GO

