/****** Object:  Table [dbo].[IVOrderShippingLine]    Script Date: 3/4/2019 7:01:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVOrderShippingLine](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IVOrderId] [int] NULL,
	[Code] [nvarchar](100) NULL,
	[Price] [numeric](14, 4) NULL,
	[Source] [nvarchar](50) NULL,
	[Title] [nvarchar](200) NULL,
	[TaxLines] [nvarchar](200) NULL,
	[CarrierIdentifier] [nvarchar](200) NULL,
 CONSTRAINT [PK_IVOrderShippingLine] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[IVOrderShippingLine]  WITH CHECK ADD  CONSTRAINT [FK_IVOrderShipping:Line_IVOrder] FOREIGN KEY([IVOrderId])
REFERENCES [dbo].[IVOrder] ([Id])
GO

ALTER TABLE [dbo].[IVOrderShippingLine] CHECK CONSTRAINT [FK_IVOrderShipping:Line_IVOrder]
GO

