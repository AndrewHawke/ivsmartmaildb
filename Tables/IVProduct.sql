/****** Object:  Table [dbo].[IVProduct]    Script Date: 3/4/2019 7:01:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVProduct](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IVCustomerId] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Brand] [nvarchar](max) NULL,
	[ModelYear] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[ProductType] [nvarchar](200) NULL,
	[Options] [nvarchar](max) NULL,
	[OptionsOrder] [nvarchar](max) NULL,
	[DealerDiscipline] [nvarchar](200) NULL,
	[Handle] [nvarchar](max) NULL,
	[IsDeactivated] [bit] NULL,
	[CreatedAt] [datetime] NULL,
	[UpdatedAt] [datetime] NULL,
	[Note] [nvarchar](max) NULL,
	[PimProductId] [int] NULL,
	[IsDistributor] [bit] NULL,
	[PriceBand] [int] NULL,
	[PublishedAt] [datetime] NULL,
	[VisibleOnline] [bit] NULL,
	[VisiblePos] [bit] NULL,
	[StockRateOfSale] [int] NULL,
	[ShopHandle] [nvarchar](max) NULL,
	[Featured] [bit] NULL,
	[OutOfPriceBand] [bit] NULL,
 CONSTRAINT [PK_IVProduct] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[IVProduct] ADD  CONSTRAINT [DF__IVProduct__IsDea__7B5B524B]  DEFAULT ((0)) FOR [IsDeactivated]
GO

ALTER TABLE [dbo].[IVProduct]  WITH CHECK ADD  CONSTRAINT [FK_IVProduct_IVCustomer] FOREIGN KEY([IVCustomerId])
REFERENCES [dbo].[IVCustomer] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[IVProduct] CHECK CONSTRAINT [FK_IVProduct_IVCustomer]
GO

