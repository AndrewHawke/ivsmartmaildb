/****** Object:  Table [dbo].[IVCustFavManual]    Script Date: 3/4/2019 6:25:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVCustFavManual](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Type] [char](1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[Value] [int] NOT NULL
) ON [PRIMARY]
GO

