/****** Object:  Table [dbo].[IVProductVariant]    Script Date: 3/4/2019 7:03:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVProductVariant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IVProductId] [int] NULL,
	[IVProductImageId] [int] NULL,
	[IVBrandColourId] [int] NULL,
	[PimProductVariantId] [int] NULL,
	[Sku] [nvarchar](50) NULL,
	[Upc] [nvarchar](max) NULL,
	[Mpn] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Weight] [decimal](11, 4) NULL,
	[WeightUnit ] [nvarchar](10) NULL,
	[Option1] [nvarchar](400) NULL,
	[Option2] [nvarchar](400) NULL,
	[Option3] [nvarchar](400) NULL,
	[Position] [int] NULL,
	[RedeemableUsingEscDollars] [bit] NULL,
	[SaleOnB2B] [bit] NULL,
	[MoqDealerA] [int] NULL,
	[MoqDealerB] [int] NULL,
	[MoqDealerC] [int] NULL,
	[ForeignCurrencyUnits] [nvarchar](10) NULL,
	[CostOfGoodsFc] [numeric](14, 4) NULL,
	[XRateBuying] [numeric](14, 4) NULL,
	[CostOfGoods] [numeric](14, 4) NULL,
	[ShippingAndDuties] [numeric](14, 4) NULL,
	[LandedCost] [numeric](14, 4) NULL,
	[MsrpFc] [numeric](14, 4) NULL,
	[XRateSelling] [numeric](14, 4) NULL,
	[Msrp] [numeric](14, 4) NULL,
	[EscapeSrp] [numeric](14, 4) NULL,
	[EscapeSrpWithGST] [numeric](14, 4) NULL,
	[EscapeSrpDisc] [numeric](14, 4) NULL,
	[EscapeSrpDiscWithGST] [numeric](14, 4) NULL,
	[EscDollarsCustomer] [numeric](14, 4) NULL,
	[RetailEscDollars] [numeric](14, 4) NULL,
	[RetailEscDollarsWithDisc] [numeric](14, 4) NULL,
	[CommissionDealerA] [numeric](14, 4) NULL,
	[CommissionDealerB] [numeric](14, 4) NULL,
	[CommissionDealerC] [numeric](14, 4) NULL,
	[DealerASalePrice] [numeric](14, 4) NULL,
	[DealerASalePriceWithGST] [numeric](14, 4) NULL,
	[EscDollarsDealerA] [numeric](14, 4) NULL,
	[DealerBSalePrice] [numeric](14, 4) NULL,
	[DealerBSalePriceWithGST] [numeric](14, 4) NULL,
	[EscDollarsDealerB] [numeric](14, 4) NULL,
	[DealerCSalePrice] [numeric](14, 4) NULL,
	[DealerCSalePriceWithGST] [numeric](14, 4) NULL,
	[EscDollarsDealerC] [numeric](14, 4) NULL,
	[CreatedAt] [datetime] NULL,
	[UpdatedAt] [datetime] NULL,
	[StockLevel] [int] NULL,
 CONSTRAINT [PK_IVProductVariant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[IVProductVariant]  WITH CHECK ADD  CONSTRAINT [FK_IVProductVariant_IVBrandColour] FOREIGN KEY([IVBrandColourId])
REFERENCES [dbo].[IVBrandColour] ([Id])
GO

ALTER TABLE [dbo].[IVProductVariant] CHECK CONSTRAINT [FK_IVProductVariant_IVBrandColour]
GO

ALTER TABLE [dbo].[IVProductVariant]  WITH CHECK ADD  CONSTRAINT [FK_IVProductVariant_IVProduct] FOREIGN KEY([IVProductId])
REFERENCES [dbo].[IVProduct] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[IVProductVariant] CHECK CONSTRAINT [FK_IVProductVariant_IVProduct]
GO

ALTER TABLE [dbo].[IVProductVariant]  WITH CHECK ADD  CONSTRAINT [FK_IVProductVariant_IVProductImage] FOREIGN KEY([IVProductImageId])
REFERENCES [dbo].[IVProductImage] ([Id])
ON DELETE SET NULL
GO

ALTER TABLE [dbo].[IVProductVariant] CHECK CONSTRAINT [FK_IVProductVariant_IVProductImage]
GO

