/****** Object:  Table [dbo].[IVOrderAddress]    Script Date: 3/4/2019 7:00:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVOrderAddress](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IVOrderId] [int] NULL,
	[Address1] [nvarchar](200) NULL,
	[Address2] [nvarchar](200) NULL,
	[City] [nvarchar](50) NULL,
	[CompanyName] [nvarchar](200) NULL,
	[Country] [nvarchar](50) NULL,
	[FirstName] [nvarchar](200) NULL,
	[LastName] [nvarchar](200) NULL,
	[Phone] [nvarchar](15) NULL,
	[State] [nvarchar](100) NULL,
	[PostalCode] [nvarchar](10) NULL,
	[Name] [nvarchar](max) NULL,
	[CountryCode] [nvarchar](2) NULL,
	[Latitude] [decimal](14, 4) NULL,
	[Longitude] [decimal](14, 4) NULL,
	[IsDefault] [bit] NULL,
 CONSTRAINT [PK_IVOrderAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[IVOrderAddress]  WITH CHECK ADD  CONSTRAINT [FK_IVOrderAddress_IVOrder] FOREIGN KEY([IVOrderId])
REFERENCES [dbo].[IVOrder] ([Id])
GO

ALTER TABLE [dbo].[IVOrderAddress] CHECK CONSTRAINT [FK_IVOrderAddress_IVOrder]
GO

