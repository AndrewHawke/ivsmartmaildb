/****** Object:  Table [dbo].[IVCustProdTypeRating]    Script Date: 3/4/2019 6:29:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVCustProdTypeRating](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ProductTypeId] [int] NOT NULL,
	[Rating] [int] NOT NULL
) ON [PRIMARY]
GO

