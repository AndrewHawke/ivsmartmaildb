/****** Object:  Table [dbo].[IVCustFavDisc]    Script Date: 3/4/2019 6:25:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVCustFavDisc](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DisciplineId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ValuePrcnt] [int] NOT NULL,
	[InStore] [bit] NOT NULL,
	[Value] [int] NOT NULL
) ON [PRIMARY]
GO

