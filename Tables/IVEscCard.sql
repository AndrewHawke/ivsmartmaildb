/****** Object:  Table [dbo].[IVEscCard]    Script Date: 3/4/2019 6:56:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVEscCard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ShopifyId] [bigint] NULL,
	[Title] [nvarchar](200) NULL,
	[Value] [decimal](10, 4) NULL,
	[CreatedAt] [date] NULL,
	[ExpiryDate] [date] NULL,
	[ExpiryPeriod] [int] NULL
) ON [PRIMARY]
GO

