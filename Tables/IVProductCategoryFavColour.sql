/****** Object:  Table [dbo].[IVProductCategoryFavColour]    Script Date: 3/4/2019 7:02:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVProductCategoryFavColour](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductCategoryId] [int] NOT NULL,
	[ColourId] [int] NOT NULL,
	[ValuePrcnt] [int] NOT NULL,
	[ValueType] [char](1) NOT NULL,
	[DisciplineId] [int] NULL,
	[Value] [int] NOT NULL,
 CONSTRAINT [PK_IVProductCategoryFavColour] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

