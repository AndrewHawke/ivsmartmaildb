/****** Object:  Table [dbo].[IVCustomer]    Script Date: 3/4/2019 6:28:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVCustomer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](400) NULL,
	[SkuAbbreviation] [nvarchar](10) NULL,
	[Domain] [nvarchar](200) NULL,
	[CustomerWebsiteUrl] [nvarchar](max) NULL,
	[BusinessWebsiteUrl] [nvarchar](max) NULL,
	[WarehouseUrl] [nvarchar](max) NULL,
	[DatabaseName] [nvarchar](200) NULL,
	[Currency] [nvarchar](50) NULL,
	[GenderEntered] [nchar](1) NULL,
	[GenderCalc] [nchar](1) NULL,
 CONSTRAINT [PK_IVCustomer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

