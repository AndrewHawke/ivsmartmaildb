/****** Object:  Table [dbo].[IVBrandFavColour]    Script Date: 3/4/2019 6:19:38 PM ******/
CREATE TABLE [dbo].[IVBrandFavColour](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BrandId] [int] NOT NULL,
	[ColourId] [int] NOT NULL,
	[ValueType] [char](1) NOT NULL,
	[ValuePrcnt] [int] NOT NULL,
	[DisciplineId] [int] NULL,
	[Value] [int] NOT NULL
) ON [PRIMARY]