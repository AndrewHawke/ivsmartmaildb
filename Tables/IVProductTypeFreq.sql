/****** Object:  Table [dbo].[IVProductTypeFreq]    Script Date: 3/4/2019 7:03:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVProductTypeFreq](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductTypeId] [int] NULL,
	[DisciplineId] [int] NULL,
	[Frequency] [int] NULL
) ON [PRIMARY]
GO

