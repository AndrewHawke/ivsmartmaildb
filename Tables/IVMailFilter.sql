/****** Object:  Table [dbo].[IVMailFilter]    Script Date: 3/4/2019 6:57:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVMailFilter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IVMailRuleId] [int] NOT NULL,
	[FilterId] [int] NULL,
	[Position] [int] NULL,
	[FilterValues] [nvarchar](500) NULL
) ON [PRIMARY]
GO

