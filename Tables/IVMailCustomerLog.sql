/****** Object:  Table [dbo].[IVMailCustomerLog]    Script Date: 3/4/2019 6:57:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVMailCustomerLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[ProductId] [int] NOT NULL,
	[VariantId] [int] NOT NULL
) ON [PRIMARY]
GO

