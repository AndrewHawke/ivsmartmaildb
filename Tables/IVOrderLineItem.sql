/****** Object:  Table [dbo].[IVOrderLineItem]    Script Date: 3/4/2019 7:01:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVOrderLineItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopifyId] [bigint] NULL,
	[Quantity] [int] NULL,
	[FulfillmentStatus] [nvarchar](50) NULL,
	[Grams] [int] NULL,
	[Price] [numeric](14, 4) NULL,
	[IVOrderId] [int] NULL,
	[RequiresShipping] [bit] NULL,
	[Sku] [nvarchar](50) NULL,
	[Brand] [nvarchar](100) NULL,
	[Title] [nvarchar](200) NULL,
	[VariantTitle] [nvarchar](200) NULL,
	[GiftCard] [bit] NULL,
	[Taxable] [bit] NULL,
	[TaxLines] [nvarchar](200) NULL,
	[TotalDiscount] [numeric](14, 4) NULL,
	[ProductTypeId] [int] NULL,
 CONSTRAINT [PK_IVOrderLineItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[IVOrderLineItem]  WITH CHECK ADD  CONSTRAINT [FK_IVOrderLineItem_IVOrder] FOREIGN KEY([IVOrderId])
REFERENCES [dbo].[IVOrder] ([Id])
GO

ALTER TABLE [dbo].[IVOrderLineItem] CHECK CONSTRAINT [FK_IVOrderLineItem_IVOrder]
GO

