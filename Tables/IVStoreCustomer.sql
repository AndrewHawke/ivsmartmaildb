/****** Object:  Table [dbo].[IVStoreCustomer]    Script Date: 3/4/2019 7:03:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVStoreCustomer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopifyId] [bigint] NULL,
	[AcceptsMarketing] [bit] NULL,
	[CreatedAt] [datetime] NULL,
	[Email] [nvarchar](50) NULL,
	[Phone] [nvarchar](15) NULL,
	[FirstName] [nvarchar](100) NULL,
	[LastName] [nvarchar](100) NULL,
	[Note] [nvarchar](max) NULL,
	[OrdersCount] [int] NULL,
	[State] [nvarchar](50) NULL,
	[TaxExempt] [bit] NULL,
	[TotalSpent] [numeric](14, 4) NULL,
	[UpdatedAt] [datetime] NULL,
	[Tags] [nvarchar](max) NULL,
	[VerifiedEmail] [bit] NULL,
	[GenderByPurchase] [nvarchar](20) NULL,
	[Gender] [nvarchar](20) NULL,
	[EscDollars] [numeric](14, 4) NULL,
	[NoFemaleOnly] [int] NULL,
	[NoChildOnly] [int] NULL,
	[NoMale] [int] NULL,
 CONSTRAINT [PK_IVStoreCustomer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

