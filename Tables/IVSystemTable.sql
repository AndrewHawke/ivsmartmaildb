/****** Object:  Table [dbo].[IVSystemTable]    Script Date: 3/4/2019 7:04:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVSystemTable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Customer] [nvarchar](400) NOT NULL,
	[MaxLevel] [int] NULL,
	[MaxBrandLevel] [int] NULL,
	[MaxPriceLevel] [int] NULL,
	[MaxColours] [int] NULL,
	[MaxDiscipline] [int] NULL,
	[DaysBeforeRePresent] [int] NULL,
	[MaxGridItems] [int] NULL,
 CONSTRAINT [PK_IVSystemTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

