﻿CREATE TABLE [dbo].[IVCategoryRelationship]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[CategoryId] INT NOT NULL,
	[ChildIsCategory] BIT,
	[ChildId] INT 
)
