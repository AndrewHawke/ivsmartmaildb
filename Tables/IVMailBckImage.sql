/****** Object:  Table [dbo].[IVMailBckImage]    Script Date: 3/4/2019 6:57:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVMailBckImage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ImageName] [nvarchar](max) NOT NULL,
	[ImageDescription] [nvarchar](max) NOT NULL,
	[ImageFileName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_IVMailBckImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

