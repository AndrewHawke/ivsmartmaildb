/****** Object:  Table [dbo].[IVProductTypePriceBand]    Script Date: 3/4/2019 7:03:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVProductTypePriceBand](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductTypeId] [int] NULL,
	[Level] [int] NULL,
	[HighValue] [int] NULL
) ON [PRIMARY]
GO

