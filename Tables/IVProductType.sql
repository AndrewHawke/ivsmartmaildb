/****** Object:  Table [dbo].[IVProductType]    Script Date: 3/4/2019 7:02:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVProductType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[RedeemableUsingEscDollars] [bit] NOT NULL,
	[OptionsCsv] [nvarchar](max) NULL,
	[VolumetricWeight] [int] NULL,
	[DealerCYes] [bit] NULL,
	[TagColour] [nvarchar](max) NULL,
	[TagMaterial] [nvarchar](max) NULL,
	[TagGender] [nvarchar](max) NULL,
	[TagDiscipline] [nvarchar](max) NULL,
	[TagDealerDiscipline] [nvarchar](max) NULL,
	[VariantBrandColour] [nvarchar](max) NULL,
	[VariantSize] [nvarchar](max) NULL,
	[VariantLength] [nvarchar](max) NULL,
	[VariantRise] [nvarchar](max) NULL,
	[VariantTravel] [nvarchar](max) NULL,
	[VariantType] [nvarchar](max) NULL,
 CONSTRAINT [PK_IVProductType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

