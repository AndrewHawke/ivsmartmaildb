/****** Object:  Table [dbo].[IVCustFavColour]    Script Date: 3/4/2019 6:25:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVCustFavColour](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ColourId] [int] NOT NULL,
	[Type] [char](1) NOT NULL,
	[TypeId] [int] NULL,
	[ValuePrcnt] [int] NOT NULL,
	[InStore] [bit] NOT NULL,
	[Value] [int] NOT NULL,
 CONSTRAINT [PK_IVCustFavColour] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

