/****** Object:  Table [dbo].[IVDisciplineProductSet]    Script Date: 3/4/2019 6:29:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVDisciplineProductSet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DisciplineId] [int] NOT NULL,
	[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO

