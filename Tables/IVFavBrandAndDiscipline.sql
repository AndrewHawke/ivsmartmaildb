/****** Object:  Table [dbo].[IVFavBrandAndDiscipline]    Script Date: 3/4/2019 6:56:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVFavBrandAndDiscipline](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [char](1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[CategoryId] [int] NULL,
	[ValuePrcnt] [int] NOT NULL,
	[Value] [int] NULL
) ON [PRIMARY]
GO

