/****** Object:  Table [dbo].[IVOrder]    Script Date: 3/4/2019 7:00:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopifyId] [bigint] NOT NULL,
	[AppId] [bigint] NOT NULL,
	[OrderName] [nvarchar](100) NULL,
	[IVCustomerId] [int] NULL,
	[IVStoreCustomerId] [int] NULL,
	[IVBillingAddressId] [int] NULL,
	[AcceptsMarketing] [bit] NULL,
	[CancelReason] [nvarchar](50) NULL,
	[CancelAt] [datetime] NULL,
	[Currency] [nvarchar](10) NULL,
	[CustomerId] [int] NULL,
	[CustomerLocale] [nvarchar](20) NULL,
	[OrderDiscountCodes] [nvarchar](200) NULL,
	[Email] [nvarchar](100) NULL,
	[FinancialStatus] [nvarchar](50) NULL,
	[FulfillmentStatus] [nvarchar](50) NULL,
	[Tags] [nvarchar](200) NULL,
	[LocationId] [bigint] NULL,
	[OrderNote] [nvarchar](max) NULL,
	[OrderNumber] [int] NOT NULL,
	[PaymentGatewayNames] [nvarchar](200) NULL,
	[Phone] [nvarchar](15) NULL,
	[ProcessedAt] [datetime] NULL,
	[ProcessingMethod] [nvarchar](20) NULL,
	[ReferringSite] [nvarchar](max) NULL,
	[IVShippingAddressId] [int] NULL,
	[SourceName] [nvarchar](20) NULL,
	[SubTotalPrice] [numeric](14, 4) NULL,
	[TaxesIncluded] [bit] NULL,
	[TotalDiscounts] [numeric](14, 4) NULL,
	[Token] [nvarchar](200) NULL,
	[TotalLineItemsPrice] [numeric](14, 4) NULL,
	[TotalPrice] [numeric](14, 4) NULL,
	[TotalTax] [numeric](14, 4) NULL,
	[TotalWeight] [bigint] NULL,
	[UpdatedAt] [datetime] NULL,
	[UserId] [bigint] NULL,
	[OrderStatusUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_IVOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[IVOrder]  WITH CHECK ADD  CONSTRAINT [FK_IVOrder_IVCustomer] FOREIGN KEY([IVCustomerId])
REFERENCES [dbo].[IVCustomer] ([Id])
GO

ALTER TABLE [dbo].[IVOrder] CHECK CONSTRAINT [FK_IVOrder_IVCustomer]
GO

