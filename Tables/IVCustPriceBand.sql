/****** Object:  Table [dbo].[IVCustPriceBand]    Script Date: 3/4/2019 6:29:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IVCustPriceBand](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CategoryId] [int] NULL,
	[Value] [int] NOT NULL,
	[InStore] [bit] NOT NULL
) ON [PRIMARY]
GO

